let fs = require('fs');

module.exports = function (self) {
  let next = this.function;
  fs.readFile(self.path, {encoding: 'utf8'}, (err, contents) => {
    next(err || contents);
  });
};
